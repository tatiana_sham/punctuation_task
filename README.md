# punctuation_task
**Задача восстановления пунктуации для русского языка**

Датасет составлен из нескольких произведений Достоевского, убраны названия частей и глав, сноски. Диалоги с новой строки идут без тире. Текст разбит на куски, оканчивающиеся на точку, вопрос или восклицательный знак.
На вход сети подаются предложения без знаков препинания, на выходе - последовательность тегов, обозначающая расположение знаков препинания. Модель (CNN + Bidirectional GRU) учится предсказывать знаки препиная как тэги.

Эмбеддинги для слов предобученные взяты [отсюда](https://github.com/natasha/navec). В процессе дообучаются.
Основано на [этом](https://github.com/LeoTSH/cnn_lstm_pos_tagger) репозитории. 

Что можно сделать:
Применить лемматизацию, но оставить генерацию с исходными формами слов.
Улучшить качество. Больше данных. Другая архитектура.