import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
import json, io
from string import punctuation
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

def plot_confusion_matrix(cm, dataset_type, normalize=False, cmap=plt.cm.Blues):
    '''
    Description: 
        - Prints and plots the confusion matrix. Normalization can be applied by setting `normalize=True`

    Args:
        - cm: Confusion Matrix
        - classes: Names of classes
        - normalize: Whether to or to not normal values in Confusion Matrix
        - cmap: Plot color
    '''

    # Check if normalize is true or false
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("****Normalized confusion matrix:****")
    else:
        print('Confusion matrix, without normalization')
    
    # Define class names
    if dataset_type == 'mge' or dataset_type == 'MGE':
        classes = ['Pad', 'NA', 'Period', 'Comma', 'Question', 'Exclaim', 'Ellipsis']        
    else:
        classes = ['Pad', 'NA', 'Comma', 'Period', 'Question', 'Exclaim', 'Ellipsis']      
        
    # Format axis and plot Confusion Matrix
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(dataset_type+' Confusion Matrix')
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                horizontalalignment="center",
                color="white" if cm[i, j] > thresh else "black")
    plt.show()
#     plt.savefig('test1.jpg')

def get_labels(seq):
    '''
    Description: 
        - Creates a sequence of labels based on the input sequence

    Args:
        - seq: Input sequence
    
    Returns:
        - labels_seq: Sequence labels
    '''
    
    labels_seq = []
    seq = seq.split()
    for i in range(len(seq)):
        if '...' in seq[i]:
            labels_seq.append('<3-dots>')
        elif ',' in seq[i]:
            labels_seq.append('<comma>')
        elif '.' in seq[i]:
            labels_seq.append('<period>')
        elif '?' in seq[i]:
            labels_seq.append('<question>')
        elif '!' in seq[i]:
            labels_seq.append('<exclaim>')
        elif '–' in seq[i]:
            labels_seq.append('<dash>')
        elif ':' in seq[i]:
            labels_seq.append('<colon>')
        elif ';' in seq[i]:
            labels_seq.append('<semicolon>')
#         elif '"' in seq[i]:
#             labels_seq.append('<quotes>')
#         elif '(' in seq[i]:
#             labels_seq.append('<bra>')
#         elif ')' in seq[i]:
#             labels_seq.append('<ket>')
        else:
            labels_seq.append('<na>')
    return labels_seq


def process_dataset(dataset_name):
    '''
    Description:
        - Process the Ted Talks dataset
        - Processed data and labels are written to the processed folder
        
    Args:
        - dataset_name: Name of dataset file to be processed
    '''
    data = open('./data/raw/'+dataset_name, 'r', encoding='utf-8').read()
    
    # Convert all characters to lowercase
    data = data.lower()

    # Look-up table to remove punctuations from data
    table = str.maketrans('', '', punctuation)

    # Define and remove characters and bracketed actions
    replace = ['♫', '♪', '…', '(applause)', '(laughter)']
    for i in range(len(replace)):
        data = data.replace(replace[i], '')

    # Split dataset by sentences
    data_split = data.split('\n')
    print('Pre number of '+dataset_name+' sentences: \t{}'.format(len(data_split)))
    print('\n')
    
    # Get longest sentence in dataset and its index
    print(max(enumerate(data_split), key=lambda x: len(x[1])))
    print('\n')
    print('Length of longest '+dataset_name+' sentence: \t{}'.format(len(max(data_split, key=len))))

    # Clean and split the longest sentence into multiple ones based on full-stops
    # data_split[3223] = data_split[3223].replace(',', ', ')
    # data_split[3223] = data_split[3223].replace('.', '.\n')
    # long_sent = data_split[3223].split('\n')

    # # Check number of sentences from chunking longest sentence
    # print('Chunked longest '+dataset_name+' sentence: \t{}'.format(len(long_sent)))

    # Remove longest sentence at index 185703
    # del data_split[3223]

    # Add chunked sentences back to dataset
    # for x in long_sent:
        # data_split.append(x)

    # Check length of dataset after addition
    print('Post number of '+dataset_name+' sentences: \t{}'.format(len(data_split)))
    print('\n')

    # Remove empty rows
    data_split = data_split[:238003]

    # Check last sentence of dataset
    print('Last Sentence of '+dataset_name+' dataset: {}'.format(data_split[-1]))
    print('\n')

    # Get corresponding labels for dataset
    process_labels = [get_labels(seq) for seq in data_split]
    process_labels = [' '.join(seq) for seq in process_labels]

    # Remove all punctuations from dataset
    sequences = [seq.translate(table) for seq in data_split]

    # Combined sentences back into a single piece for Counter
    combined_sequences = ' '.join(sequences)

    # Check if there are additional characters to remove
#     print(Counter(combined_sequences))
#     print('\n')

    # Get all words in the dataset
    words = combined_sequences.split()

    # Save inputs and labels for loading
    with open('./data/processed/'+dataset_name+'_processed_inputs', 'w', encoding='utf-8') as f:
        for x in sequences:
            f.write(x+'\n')
    with open('./data/processed/'+dataset_name+'_processed_labels', 'w', encoding='utf-8') as f:
        for x in process_labels:
            f.write(x+'\n')

    # Check number of sequences and labels
    print('Number of '+dataset_name+' sequences after processing: \t{}'.format(len(sequences)))
    print('Number of '+dataset_name+' labels after processing: \t{}'.format(len(process_labels)))
        
def load_processed_dataset(dataset_name):
    '''
    Description:
        - Loads processed dataset (Data and labels)
        - Dataset(s) must be present in the processed folder
        
    Args:
        - dataset_name: Name of data and labels file to be loaded
        
    Returns:
        - data_split: Processed data
        - y_labels: Processed labels
    '''
    # Load and process input/label data
    data = open('./data/processed/'+dataset_name+'_processed_inputs', 'r', encoding='utf-8').read()
    data = data.lower()
    data_split = data.split('\n')
    data_split = data_split[:-1]

    # Load processed labels
    y_labels = open('./data/processed/'+dataset_name+'_processed_labels', 'r', encoding='utf-8').read()
    y_labels = y_labels.split('\n')
    y_labels = y_labels[:-1]
    
    return data_split, y_labels    

def tokenize_data(data, labels, dataset_name, maxlen, seq_lab_padding):
    '''
    Description:
        - Tokenize and pad data/labels into integer sequences
        - Labels will further be one-hot encoded
        - Json files mapping unique words and labels will also be written into the processed folder
        
    Args:
        - data: Input data
        - labels: Labels
        - dataset_name: Name of dataset
        
    Returns:
        - unique_vocab: Number of unique words
        - no_classes: Number of unique labels
        - pad_seq: Tokenized and padded input data
        - encoded_labels: One-hot encoded labels sequences
        - vocab_to_int: Mapping of unique words to integers
        - label_to_int: Mapping of unique labels to integers
    '''
    # Join all words in data into one
    all_data = ' '.join(data)
    # Get all words
    words = all_data.split()
        
    # Build words vocab
    words_in_vocab = Counter(words)
    vocab = sorted(words_in_vocab, key=words_in_vocab.get, reverse=True)

    # Skip most common word
    vocab_to_int = {word: index for index, word in enumerate(vocab, 2)}
    vocab_to_int['<pad>'] = 0  # The special value used for padding
    vocab_to_int['<oov>'] = 1  # The special value used for OOVs
    
    # Write vocab dictionaries to file
    with open('./data/processed/'+dataset_name+'_vocabs.json', 'w', encoding='utf-8') as fv:
        json.dump(vocab_to_int, fv, indent=4)
    
    # Check number of unique words
    unique_vocab = len(vocab_to_int)
    print('Number of unique words:', unique_vocab)
    print('\n')
    
    # Tokenize input sequences
    seq_int = []
    for seq in data:
        seq_int.append([vocab_to_int[word] for word in seq.split()])

    # Pad input sequences
    pad_seq = pad_sequences(sequences=seq_int, maxlen=maxlen, padding=seq_lab_padding, value=0)

    # Check sample sequence
    print('Sample sequence:', data[-1])
    print('\n')
    print('Sample sequence:', pad_seq[-1])
    print('\n')
    
    # Join all labels into one
    all_labels = ' '.join(labels)
    # Get all labels
    labels_tag = all_labels.split()
    
    print('Labels: {}'.format(Counter(labels_tag)))
    
    # Build labels vocab
    labels_in_vocab = Counter(labels_tag)
    labels_vocab = sorted(labels_in_vocab, key=labels_in_vocab.get, reverse=True)
    label_to_int = {t: i for i, t in enumerate(labels_vocab, 1)}
    label_to_int['<pad>'] = 0  # The special value used to padding #variable

    # Write labels dictionaries to file
    with open('./data/processed/'+dataset_name+'_labels.json', 'w', encoding='utf-8') as fl:
        json.dump(label_to_int, fl, indent=4)

    # Check label classes distribution
    no_classes = len(label_to_int)
    print('Class distribution:', Counter(labels_in_vocab))
    print('\n')

    # Check number of unique labels
    print('Number of unique labels:', no_classes)
    print(label_to_int)   
    print('\n')

    # Tokenize output labels
    lab_int = []
    for lab in labels:
        lab_int.append([label_to_int[word] for word in lab.split()])

    # Pad input labels
    pad_labels = pad_sequences(sequences=lab_int, maxlen=maxlen, padding=seq_lab_padding, value=0)
    encoded_labels = [to_categorical(i, num_classes=no_classes) for i in pad_labels]

    # Check sample label
#     print('Sample label: \t{}'.format(pad_labels[-1]))
#     print('\n')
#     print('Encoded label: \t{}'.format(encoded_labels[-1]))
#     print('\n')

    # Check that all sequences and labels are at max sequence length 
    assert len(pad_seq)==len(seq_int)
    assert len(pad_seq[0])==maxlen

    assert len(pad_labels)==len(lab_int)
    assert len(pad_labels[0])==maxlen
    print('Sequence and labels length check passed!')
    
    return unique_vocab, no_classes, pad_seq, encoded_labels, vocab_to_int, label_to_int

def split_data(pad_seq, encoded_labels, dataset_name, split_rat):
    '''
    Description:
        - Splits tokenized data and labels into training and validation sets
        
    Args:
        - pad_seq: Toekenized input data sequences
        - encoded_labels: One-hot encoded labels sequences
        - dataset_name: Name of dataset
        
    Returns:
        - train_val_x: Training/validation input data
        - test_x: Testing input data
        - train_val_y: Training/validation labels
        - test_y: Testing labels
    '''
    # Ratio to split data and labels at
    split_index = int(split_rat*len(pad_seq))

    # Split data into training, validation, and test data (features and labels, x and y)
    train_val_x, test_x = pad_seq[:split_index], pad_seq[split_index:]
    train_val_y, test_y = encoded_labels[:split_index], encoded_labels[split_index:]

    # print out the shapes of your resultant feature data
    print('Training/Validation '+dataset_name+' Dataset: \t{}'.format(train_val_x.shape), len(train_val_y))
    print('Testing '+dataset_name+' Dataset: \t\t\t{}'.format(test_x.shape), len(test_y))
    
    return train_val_x, test_x, train_val_y, test_y
